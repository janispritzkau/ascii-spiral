#include <stdio.h>
#include <math.h>
#include <string>
#include <sys/ioctl.h>

const std::string chars = "   .*+csyP";

/**
    https://swiftcoder.wordpress.com/2010/06/21/logarithmic-spiral-distance-field/
*/
float spiral_distance(float x, float y, float a = 1.3, float b = 0.125) {
    float r = std::sqrt(x*x + y*y);
    float t = std::atan2(y, x);

    if (r == 0.0) return 0.0;

    float n = (std::log(r/a)/b - t) / (M_PI * 2);

    float upper_r = a * std::pow(M_E, b * (t + 2.0*M_PI*std::ceil(n)));
    float lower_r = a * std::pow(M_E, b * (t + 2.0*M_PI*std::floor(n)));

    return std::min(std::abs(upper_r - r), std::abs(r - lower_r));
}

float get_char_val(float x, float y) {
    y += 0.24;
    float d = spiral_distance(x, y);

    float v = std::fmax(0.7 - d, 0.0) / 0.7;
    v = std::pow(v, 6.0);

    float m = 1 / (1 + std::pow(std::sqrt(x*x + y*y) * 0.63, 10.0));
    v *= m;

    return v;
}

int main(int argc, char const *argv[]) {
    int width = 60, height = 26;

    // if option '-f' is provided set width and height to terminal size
    if (argc > 1 && argv[1] == std::string("-f")) {
        struct winsize ws;
        ioctl(0, TIOCGWINSZ, &ws);

        width = ws.ws_col;
        height = ws.ws_row - 1;
    }

    // aspect ratio and character width scaling
    float scale_x = (float)width / (float)height * 0.5;
    float scale = 3.4;

    for (auto row = 0; row < height; row++) {
        for (auto col = 0; col < width; col++) {
            float x = (float(col) / float(width) - 0.5) * scale * scale_x;
            float y = (float(row) / float(height) - 0.5) * scale;

            int idx = int(get_char_val(x, y) * chars.length() - 0.5);
            printf("%c", chars[idx]);
        }
        printf("\n");
    }
}
