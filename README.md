# ASCII Spiral Generator

Creates a spiral with ascii characters using distance functions.

![Spiral](spiral.png)
